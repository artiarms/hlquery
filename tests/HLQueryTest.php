<?php

use CSParadise\HLQuery\HLQuery;

class HLQueryTest extends PHPUnit_Framework_TestCase
{
    protected $hlquery;

    protected function setUp()
    {
        $this->hlquery = new HLQuery(getenv('HLDS_IP'), getenv('HLDS_PORT'), getenv('HLDS_RCON_PASSWORD'));
    }

    protected function tearDown()
    {
        $this->hlquery = null;
    }

    public function testGetInfo()
    {
        $info = $this->hlquery->getInfo();

        $this->assertTrue(is_array($info));
    }
}
