<?php
namespace CSParadise\HLQuery;

class HLQuery
{
    /**
     * Socket connection.
     *
     * @var resource
     */
    private $socket;

    /**
     * @var int
     */
    private $challengeNumber = 0;

    /**
     * RCON password.
     *
     * @var bool
     */
    protected $connected = false;

    /**
     * RCON password.
     *
     * @var string
     */
    protected $password;

    /**
     * Constructor.
     *
     * @param string     $ip
     * @param string|int $port
     * @param string     $password
     */
    public function __construct($ip, $port = 27015, $password = '')
    {
        $this->socket = fsockopen('udp://' . $ip, $port, $errno, $errstr, 2);
        stream_set_timeout($this->socket, 1);

        $this->connected = !!$this->socket;
        $this->password  = $password;
    }

    /**
     * Closes the connection with the HL server.
     */
    public function __destruct()
    {
        fclose($this->socket);

        $this->connected = false;
    }

    /**
     * Returns state of the connection with the HL server.
     */
    public function isConnected()
    {
        return $this->connected;
    }

    /**
     * Gets info via "stats" command.
     *
     * @return array|null
     */
    public function getStats(): ?array
    {
        $srcData = $this->sendRconCommand('stats');

        if (empty($srcData)) {
            return null;
        }

        $srcData = explode("\n", $srcData);
        $dstData = [];

        [
            $dstData['cpu'],
            $dstData['in'],
            $dstData['out'],
            $dstData['uptime'],
            $dstData['users'],
            $dstData['fps'],
            $dstData['players']
        ] = explode("\t", preg_replace('/\s+/', "\t", trim($srcData[1])));

        return $dstData;
    }

    /**
     * Gets detailed player info via "status" command.
     *
     * @return bool|string
     */
    public function getInfo()
    {
        $response = $this->sendRconCommand('status');

        // Format global server info
        $line = explode("\n", $response);
        $map = substr($line[3], strpos($line[3], ':') + 1);
        $players = trim(substr($line[4], strpos($line[4], ':') + 1));
        $active = explode(' ', $players);

        $result['ip'] = trim(substr($line[2], strpos($line[2], ':') + 1));
        $result['name'] = trim(substr($line[0], strpos($line[0], ':') + 1));
        $result['map'] = trim(substr($map, 0, strpos($map, 'at:')));
        $result['mod'] = 'Counter-Strike ' . trim(substr($line[1], strpos($line[1], ':') + 1));
        $result['game'] = 'Halflife';
        $result['curplayers'] = $active[0];
        $result['maxplayers'] = substr($active[2], 1);
        $result['players'] = array();

        for ($i = 1; $i <= $result['curplayers']; $i++) {
            // Get possible player line
            $tmp = $line[$i + 6];

            // Break if no more players are left
            if (substr_count($tmp, '#') <= 0)
                break;

            // Парсинг с учетом особенности информации о HLTV
            // #   name                     userid  uniqueid                frag                time        ping    loss    adr
            // # 1 "HLTV Paradise Place"    1       HLTV                    hltv:0/10 delay:0   14:33:09                    46.174.48.32:28251
            // # 2 "Ruslan A."              2       STEAM_0:0:21140002      0                   33:30       5       0       109.72.73.8:27005
            preg_match('/#\s?([0-9]{1,2})\s+"(.*)" (\d+) ([\w:]+)\s+(-?\d+|hltv:0\/10 delay:0)\s+([\d+:?\d+:\d+]+)\s+((\d+)\s+)?((\d+)\s+)?(.*)/i', $tmp, $data);

            $player = [
                'index'  => (int) $data[1],
                'name'   => $data[2],
                'userid' => (int) $data[3],
                'authid' => $data[4],
                'score'  => (int) $data[5],
                'time'   => $data[6],
                'ping'   => (int) $data[7],
                'loss'   => (int) $data[9],
                'ip'     => explode(':', $data[11])[0]
            ];

            $time = explode(':', $player['time']);
            $time_segments = count($time);

            switch ($time_segments) {
                case 1: // s
                    $player['seconds'] = $time[0];
                    break;
                case 2: // m:s
                    $player['seconds'] = ($time[0] * 60) + $time[1];
                    break;
                case 3: // h:m:s
                    $player['seconds'] = ($time[0] * 60 * 60) + ($time[1] * 60) + $time[2];
                    break;
            }

            $result['players'][$i] = $player;
        }

        return $result;
    }

    /**
     * Gets list of maps via "maps" command.
     *
     * @param  int  $pageNumber
     * @return array
     */
    function getMapsList($pageNumber = 0) {
        if (!$this->connected) {
            return $this->connected;
        }

        $maps = $this->sendRconCommand('maps *', $pageNumber);

        if (!$maps || trim($maps) == 'Bad rcon_password.') {
            return $maps;
        }

        $maps = explode(PHP_EOL, $maps);
        $mapsList = array();

        foreach ($maps as $map) {
            if ($map == '-------------') {
                continue;
            }

            $map = str_replace('.bsp', '', $map);
            $mapsList[] = $map;
        }

        return $mapsList;
    }

    /**
     * Get server info via info protocol.
     *
     * @return array
     */
    function Info() {
        //If there is no open connection return false
        if (!$this->connected)
            return $this->connected;

        //send info command
        $command = "\xff\xff\xff\xffTSource Engine Query\x00";
        $buffer = $this->Communicate($command);

        //If no connection is open
        if (trim($buffer) == '') {
            $this->connected = false;
            return false;
        }

        // Build info array
        $pos = 0;
        $result['type'] = $this->parseBuffer($buffer, $pos, 'bytestr');

        if ($result['type'] == 'I') {
            $result['version']	  = $this->parseBuffer($buffer, $pos, 'byte');
            $result['name'] 	  = $this->parseBuffer($buffer, $pos, 'string');
            $result['map']		  = $this->parseBuffer($buffer, $pos, 'string');
            $result['mod']		  = $this->parseBuffer($buffer, $pos, "string");
            $result['game']		  = $this->parseBuffer($buffer, $pos, "string");
            $result['appid']	  = $this->parseBuffer($buffer, $pos, 'short');
            $result['players']	  = $this->parseBuffer($buffer, $pos, 'byte');
            $result['maxplayers'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result['botplayers'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result['dedicated']  = $this->parseBuffer($buffer, $pos, 'bytestr');
            $result['os']		  = $this->parseBuffer($buffer, $pos, 'bytestr');
            $result['password']	  = $this->parseBuffer($buffer, $pos, 'byte');
            $result['secure']	  = $this->parseBuffer($buffer, $pos, 'byte');
            $result['sversion']	  = $this->parseBuffer($buffer, $pos, 'string');
            $result['edf']		  = $this->parseBuffer($buffer, $pos, 'byte');

            switch ($result['edf']) {
                case '\x80': // The server's game port # is included
                    $result['port'] = $this->parseBuffer($buffer, $pos, 'short');
                    break;
                case '\x40': // The spectator port # and then the spectator server name are included
                    $result['specport'] = $this->parseBuffer($buffer, $pos, 'short');
                    $result['specservername'] = $this->parseBuffer($buffer, $pos, 'string');
                    break;
                case '\x20': // The game tag data string for the server is included [future use]
                    $result['gametagdata'] = $this->parseBuffer($buffer,$pos,"string");
            }
        } else {
            $result['address'] = $this->parseBuffer($buffer, $pos, 'string');
            $result['name'] = $this->parseBuffer($buffer, $pos, 'string');
            $result['map'] = $this->parseBuffer($buffer, $pos, 'string');
            $result['mod'] = $this->parseBuffer($buffer, $pos, 'string');
            $result['game'] = $this->parseBuffer($buffer, $pos, 'string');
            $result['players'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result['maxplayers'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result['protocol'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result['dedicated'] = $this->parseBuffer($buffer, $pos, 'bytestr');
            $result['os'] = $this->parseBuffer($buffer, $pos, 'bytestr');
            $result['password'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result['modrunning'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result['modurl'] = $this->parseBuffer($buffer, $pos, 'string');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $this->parseBuffer($buffer, $pos, 'byte');
            $result['secure'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result['botplayers'] = $this->parseBuffer($buffer, $pos, 'byte');
        }

        //$this->Communicate("");

        return $result;
    }

    /**
     * Gets players via info protocol.
     *
     * @return bool|array
     */
    function getPlayers() {
        if (!$this->connected) {
            return $this->connected;
        }

        // Get challenge number
        if ($this->challengeNumber == '') {
            // Send request of challenge number
            $challenge = "\xFF\xFF\xFF\xFF\x55\xFF\xFF\xFF\xFF";
            $buffer = $this->communicate($challenge);

            // If no connection is open
            if (trim($buffer) == '') {
                $this->connected = false;
                return false;
            }

            // Get challenge number
            $this->challengeNumber = substr($buffer, 1, 4);
        }

        // Send players command
        $command = "\xff\xff\xff\xff\x55$this->challengeNumber";
        $buffer = $this->communicate($command);

        // If no connection is open
        if (trim($buffer) == '') {
            $this->connected = false;
            return false;
        }

        // Get number of online players
        // $buffer = substr($buffer, 1);
        $pos = 0;
        $header = $this->parseBuffer($buffer, $pos, 'bytestr');
        $numpl  = $this->parseBuffer($buffer, $pos, 'byte');

        // Build players array
        if ($header != 'D') {
            return;
        }

        $result = [];

        for ($i = 0; $i < $numpl; $i++) {
            $result[$i]['index'] = $this->parseBuffer($buffer, $pos, 'byte');
            $result[$i]['name']  = $this->parseBuffer($buffer, $pos, 'string');
            $result[$i]['frag']  = $this->parseBuffer($buffer, $pos, 'long');
            $result[$i]['time']  = $this->parseBuffer($buffer, $pos, 'float');
        }

        return $result;
    }

    /**
     * Sends RCON command on open socket connection.
     *
     * @param  string  $command
     * @param  int     $pageNumber
     * @param  bool    $single
     * @return string
     * @throws HLQueryException  Empty response or incorrect rcon password
     */
    public function sendRconCommand($command, $pageNumber = 0, $single = true): string
    {
        if (!$this->connected) {
            return $this->connected;
        }

        $this->getChallenge();
        $command = "\xff\xff\xff\xffrcon $this->challengeNumber \"$this->password\" $command\n";

        // Get specified page
        $result = '';
        $buffer = '';

        while ($pageNumber >= 0) {
            // Send rcon command
            $buffer .= substr($this->communicate($command), 1);

            // Get only one package
            if ($single === true) {
                $result = $buffer;
            } else { // Get more then one package and put them together
                $result.= $buffer;
            }

            // Clear command for higher iterations
            $command = '';

            $pageNumber--;
        }

        $result = trim($result);

        if (empty($result)) {
            throw new HLQueryException('Empty response');
        }

        if ($result === 'Bad rcon_password.') {
            throw new HLQueryException($result);
        }

        // To get more than 1 page from rcon
        // write command on socket
        /* if ($command != '')
            fputs($this->socket, $command, strlen($command));

        // Get results from server
        $buffer = fread($this->socket, 1);
        $status = socket_get_status($this->socket);

        // Sander's fix:
        while ($status["unread_bytes"] > 0 && $timeout < 10) {
            // Get results from server
            $buffer .= fread($this->socket, $status['unread_bytes']);
            $result .= substr($buffer,5);

            echo '######'. substr($buffer, 20) . '#########<br>';

            $buffer = fread($this->socket, 1);
            $status = socket_get_status($this->socket);

            echo $status["unread_bytes"];

            $timeout++;
            echo $timeout;
        }

        echo $buffer; */

        return trim($result);
    }

    /**
     * @return bool|void
     */
    private function getChallenge()
    {
        if ($this->challengeNumber == '') {
            // Send request of challenge number
            $challenge = "\xff\xff\xff\xffchallenge rcon\n";
            $buffer = $this->communicate($challenge);

            // If no connection is open
            if (trim($buffer) == '') {
                $this->connected = false;
                return false;
            }

            // Get challenge number
            $this->challengeNumber = trim(substr($buffer, 15));
        }
    }

    /**
     * Parse buffer.
     *
     * @param  string  $buffer
     * @param  int     $pos
     * @param  string  $type
     * @return string
     */
    private function parseBuffer($buffer, &$pos, $type)
    {
        $result = '';

        switch ($type) {
            case 'string':
                while (substr($buffer, $pos, 1) !== "\x00") {
                    $result .= substr($buffer, $pos, 1);
                    $pos++;
                }
                break;
            case 'short':
                $result = ord(substr($buffer, $pos, 1)) + (ord(substr($buffer, $pos + 1, 1)) << 8);
                $pos++;
                break;
            case 'long':
                $result = ord($buffer[$pos]) + (ord($buffer[$pos + 1]) << 8) + (ord($buffer[$pos + 2]) << 16) + (ord($buffer[$pos + 3]) << 24);
                $pos += 3;
                break;
            case 'byte':
                $result = ord(substr($buffer, $pos, 1));
                break;
            case 'bytestr':
                $result = substr($buffer, $pos, 1);
                break;
            case 'float':
                $tmptime = unpack('ftime', substr($buffer, $pos, 4));
                $result = date('H:i:s', round($tmptime['time'], 0) + 82800);
                $pos += 3;
                break;
        }

        $pos++;

        return $result;
    }

    /**
     * Communicates between PHPrcon and the game server.
     *
     * @param string $command
     */
    private function communicate($command)
    {
        // If there is no open connection return false
        if (!$this->connected) {
            return $this->connected;
        }

        // Read all pending packets before sending a request
        do {
            $rfds = [$this->socket];
            $wfds = NULL;
            $efds = NULL;
            $num_changed_sockets = stream_select($rfds, $wfds, $efds, 0);

            if ($num_changed_sockets === false) {
                break;
            } else if ($num_changed_sockets === 0) {
                break;
            } else {
                $buffer = stream_socket_recvfrom($this->socket, 65536);
            }
        } while (true);

        // Write command on socket
        if ($command != "") {
            fputs($this->socket, $command, strlen($command));
        }

        // Get results from server
        $buffer = fread($this->socket, 1);
        $status = socket_get_status($this->socket);

        // Sander's fix:
        if ($status['unread_bytes'] > 0) {
            $buffer .= fread($this->socket, $status['unread_bytes']);
        }

        // If there is another package waiting
        if (substr($buffer, 0, 4) == "\xfe\xff\xff\xff") {
            // Get requestid from split packages
            $requestid = substr($buffer, 4, 4);

            // Get number of packages
            $po = ord(substr($buffer, 8, 1));
            $panum = ($po & 1) + ($po & 2) + ($po & 4) + ($po & 8);

            // Get number from current package
            $po = $po >> 4;
            $pacur = ($po & 1) + ($po & 2) + ($po & 4) + ($po & 8);

            // Add the first package to the array
            if ($pacur == ($panum - 1)) {
                $splitbuffer[$pacur] = substr($buffer, 9);
            } else {
                $splitbuffer[$pacur] = substr($buffer, 14);
            }

            // Get all missing packages, the fist one we have, so start with 1
            for ($i = 1; $i<$panum; $i++) {
                // Get next package
                $buffer2 = fread ($this->socket, 1);
                $status  = socket_get_status($this->socket);
                $buffer2.= fread($this->socket, $status['unread_bytes']);

                // Get number from current package
                $requestid2 = substr($buffer, 4, 4);
                $po = ord(substr($buffer2, 8, 1));
                $po = $po >> 4;
                $pacur= ($po & 1) + ($po & 2) + ($po & 4) + ($po & 8);

                // Check the requestid from every package and add to array
                if ($requestid == $requestid2) {
                    if ($pacur == ($panum - 1)) {
                        $splitbuffer[$pacur] = substr($buffer2, 9);
                    } else {
                        $splitbuffer[$pacur] = substr($buffer2, 14);
                    }
                }
            }

            //add to main packet, the array is ordered by package num
            for ($i = 0; $i < $panum; $i++) {
                $bufferret .= $splitbuffer[$i];
            }
        } else { // In case there is only one package
            $bufferret = substr($buffer, 4);
        }

        // Return complete package including the type byte
        return $bufferret;
    }
}
